<?php
    date_default_timezone_set('Asia/Ho_Chi_Minh');
    $gender = array("Nam", "Nữ");
    $department = array("MAT" => "Khoa học Máy tính", "KDL" => "Khoa học vật liệu");
    echo "<!DOCTYPE html> 
    <html lang='vn'> 
    <head><meta charset='UTF-8'></head> 
    <body>\n";
    
    echo "<center>";
    echo "<fieldset style='width: 500px; height: 300px;'>";
    echo "<form style='margin: 20px 0px 0px 0px; width: 100%'>
            <table style='border-collapse:separate; border-spacing:15px 15px; margin: 0px 40px 0 35px; width: 500px'>
            <tr height='40px'>
                <td style='background-color: #5b9bd5; text-align: left; padding: 5px 5px 5px 15px'><label style='color: white'>Họ và tên</label></td>
                <td><input type='name' style='line-height: 32px; color: black; width: 100%;border: 2px solid #5b9bd5'></td>
            </tr>
            <tr height = '40px'>
                <td style='background-color: #5b9bd5; text-align: left; padding: 5px 5px 5px 15px'><label style='color: white'>Giới tính</label></td>
                <td>
        ";
    for ($i = 0; $i < count($gender); $i++)
    {
        echo "<input type='radio' name='gender' value=$i><label style='color: black'>$gender[$i]</label>";
    }
    echo "</td>";
    echo "</tr>
            <tr height = '40px'>
                <td style='background-color: #5b9bd5; text-align: left; padding: 5px 5px 5px 15px'><label style='color: white'>Phân khoa</label></td>
                <td>
                    <select id='khoa' name='Khoa' style='color: black; width: 100%; height: 40px; border: 2px solid #5b9bd5'>
                        <option value=' '></option>
        ";
    foreach ( $department as $key => $value )
    {
        echo "<option value='$key'>$value</option>";
    }
    echo "
                    </select>
                </td>
            </tr>
            </table>
            <button style='background-color: #70ad47; font-family: none; font-size: 16px; border-radius: 10px; width: 35%; height: 43px; border-width: 0; margin: 20px 130px; color: white'>Đăng ký</button>
    </form>
    </fieldset>
    </body>\n";
    echo "</center>";
    echo "</html>";
?>
